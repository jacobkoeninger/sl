console.log('Test');
let TL_Text = [];
TL_Text[1] = "Text 1";
TL_Text[2] = "Text 2";
TL_Text[3] = "Text 3";
TL_Text[4] = "Text 4";

function tl(id) {
    document.getElementById('TL-P').innerText = TL_Text[id];
}

/*  */

let Dynamic_Content = {
    "content":[
        {
            "Title":"Engagement",
            "Description":"We provide you the tools for digital engagement with your community.",
            "Outcomes":[
                "Your community will trust you more",
                "You will make better decisions and do a better job of addressing all the issues",
                "Your community will become more aware and have 2 way communication"
            ]
        },
        {
            "Title":"Wayfinding",
            "Description":"smartLINK works with google and 3d mapping technologies to provide the most top wayfind experience on the market.",
            "Outcomes":[
                "Visitors will experience Multi Modul awareness with biking, ride share, train, bus, walk and drive routes.",
                "Your community will experience increased exploration",
                "Indoor 3d navigation will provide a wonderful new experience to your visitors"
            ]
        },
        {
            "Title":"Intelligence",
            "Description":"smartLINK brings together sensor, touch and edge analytics into 1 place.",
            "Outcomes":[
                "Discover insights from the people that walk and drive the streets",
                "Enhance your decision making power with true computed data",
                "Bring in mobile and surveys to provide even more feedback into the equation."
            ]
        }
    ]
};

function dc(id) {
    console.log(Dynamic_Content.content[id].Title);   
}